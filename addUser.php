<?php
	require './header.php';
	require './include/saveUser.php';
?>
<div class="addUser">
<h3>Add User</h3>
<form action="" method="post">
	<div class="form-group">
		<div class="form-group row">
			<label for="name"class="col-2 col-form-label">Company Name:</label><br>
		    <div class="col-10">
   			  <input class="form-control" type="text" id="name" name="name" required="">
			</div>
		</div>

		<div class="form-group row">
		  <label for="fname"class="col-2 col-form-label">First name:</label>
		  <div class="col-10">
		  <input type="text" class="form-control" id="fname" name="fname"required="">
		  </div>
		</div>

		<div class="form-group row">
		  <label for="lname"class="col-2 col-form-label">Last name:</label>
		  <div class="col-10">
		  <input type="text" class="form-control"class="form-control" id="lname" name="lname"required="">
		  </div>
		</div>

		<div class="form-group row">
		  <label for="email"class="col-2 col-form-label">Email:</label>
		  <div class="col-10">
		  <input type="email"class="form-control" id="email" name="email"required="">
		  </div>
		</div>

		<div class="form-group row">
                <label for="exampleSelect2" class="col-2 col-form-label">Select City</label>
                <div class="col-10">
                <select  class="form-control" name="city" id="city" required>
                	<?php
						$getCitys = getCitys($conn);
						if (sizeof($getCitys)!=0) {
 						foreach ($getCitys as $key => $value) {
					    	?>
					    	<option value=<?php echo $value; ?> ><?php echo $value; ?></option>
					    <?php
						    }
						}
						else{
							?>
							<option>No City Found</option>
							<?php
						}
					    ?>
                	?>
                </select>
            </div>
            </div>

		<div class="form-group row">
		  <label for="street"class="col-2 col-form-label">Street:</label>
		  <div class="col-10">
		  <input type="text"class="form-control" id="street" name="street" required="">
		  </div>
		</div>

		<div class="form-group row">
		  <label for="zipcode"class="col-2 col-form-label">Zipcode:</label>
		  <div class="col-10">
		  <input type="text"class="form-control" id="zipcode" name="zipcode"required="">
		  </div>
		</div>

	</div>
	<button class="btn btn-outline-primary" type="submit" name="add_user_submit">Submit</button>
	  <a href='userList.php'>Go to list</a>
</form>
</div>
<?php
require './footer.php';
$conn->close();
?>