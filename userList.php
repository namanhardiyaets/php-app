<?php
require './header.php';
require './include/saveUser.php';

$query = "SELECT * FROM records";
if ($result = $conn->query($query)) {

if ($result->num_rows==0) {    //IF NO RECORD FOUND
  echo '<div class="noRecord"><h3>No Record Found Please Add First</h3></div>';
  echo '<button class=btn btn-outline-secondary><a href=addUser.php>Add New</a></button>';
  
}else{
   echo '<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
      <tr>
          <td> <font face="Arial">Company Name</font> </td>
          <td> <font face="Arial">First Name</font> </td>
          <td> <font face="Arial">Last Name</font> </td>
          <td> <font face="Arial">Email</font> </td>
          <td> <font face="Arial">City</font> </td>
          <td> <font face="Arial">Street</font> </td>
          <td> <font face="Arial">Zipcode</font> </td>
          <td> <font face="Arial">Action</font> </td>
      </tr>';
      $action_delete='delete';
      $action_edit='edit';
      while ($row = $result->fetch_assoc()) {
        $fieldId = $row["id"];
        $field1name = $row["company"];
        $field2name = $row["firstname"];
        $field3name = $row["lastname"];
        $field4name = $row["email"];
        $field5name = $row["city"];
        $field6name = $row["street"];
        $field7name = $row["zipcode"];
        echo '<tr>
        <td>'.$field1name.'</td> 
        <td>'.$field2name.'</td> 
        <td>'.$field3name.'</td> 
        <td>'.$field4name.'</td> 
        <td>'.$field5name.'</td>
        <td>'.$field6name.'</td>
        <td>'.$field7name.'</td>
        <td>'.'<a onclick="return confirm(\'Are you sure?\')" href=deleteUser.php?id='.$fieldId.'&action='.$action_delete.'">delete</a><br>'.'<a href="editUser.php?id='.$fieldId.'&action='.$action_edit.'">edit</a><br>'.'</td> 
              </tr>';
        }
      $result->free();
      echo '<button class=btn btn-outline-secondary><a href=export.php?to=json>Export to JSON</a></button>';
      echo '<button class=btn btn-outline-secondary><a href=export.php?to=xml>Export to XML</a></button>';
      echo '<button class=btn btn-outline-secondary><a href=addUser.php>Add New</a></button>';

    }
}

$conn->close();

?>
<?php
	require './footer.php';
?>
<link rel="stylesheet" type="text/css" href="datatable.css">