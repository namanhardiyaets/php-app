<?php
	require './header.php';
	require './include/saveUser.php';
	$id=$_GET['id'];
	//VALIDATION ON ID IS INTEGER
	if (filter_var($id, FILTER_VALIDATE_INT)) {
		    $edit = ("SELECT * FROM `records` WHERE id = '$id'");
			$result = $conn->query($edit);
			if ($result->num_rows==1) {
				foreach($result as $row)
				{
				     $Id=$row['id'];
				     $companyName=$row['company'];
				     $firstName=$row['firstname'];
				     $lastName=$row['lastname'];
				     $email=$row['email'];
				     $City=$row['city'];
				     $Street=$row['street'];
				     $zipcode=$row['zipcode'];
				}
			}else{
	  			echo '<div class="noRecord"><h3>No Record Found Please Add First</h3></div>';
	  			echo '<button class=btn btn-outline-secondary><a href=addUser.php>Add New</a></button>';
			}
		}
		else {
			$Id='';
			     $companyName='';
			     $firstName='';
			     $lastName='';
			     $email='';
			     $City='';
			     $Street='';
			     $zipcode='';
		    echo('<div class=\'err\'><h3>Not a valid Record</h3></div>');
		    echo " <a href='userList.php'>Go to list</a>";
		}

?>

<div class="addUser">
<h3>Edit User</h3>
<form action="" method="post">
	<div class="form-group">
		<div class="form-group row">
			<label for="name"class="col-2 col-form-label">Name:</label><br>
		    <div class="col-10">
			  <input class="form-control" value="<?php echo $companyName;	  ?>" type="text" id="name" name="name" required="">
			</div>
		</div>
		<input type="hidden" value="<?php echo $Id;	  ?>" class="form-control" id="id" name="id">
		<div class="form-group row">
		  <label for="fname"class="col-2 col-form-label">First name:</label>
		  <div class="col-10">
		  <input type="text" value="<?php echo $firstName;	  ?>" class="form-control" id="fname" name="fname"required="">
		  </div>
		</div>

		<div class="form-group row">
		  <label for="lname"class="col-2 col-form-label">Last name:</label>
		  <div class="col-10">
		  <input type="text" value="<?php echo $lastName;	  ?>" class="form-control"class="form-control" id="lname" name="lname"required="">
		  </div>
		</div>

		<div class="form-group row">
		  <label for="email"class="col-2 col-form-label">Email:</label>
		  <div class="col-10">
		  <input type="email" value="<?php echo $email;	  ?>"class="form-control" id="email" name="email"required="">
		  </div>
		</div>

 		<div class="form-group row">
                <label for="exampleSelect2" class="col-2 col-form-label">Select City</label>
                <div class="col-10">
                <select  class="form-control" name="city" id="city" required>
                	<?php
						$allCity = ("select * from city");
						$result = $conn->query($allCity);
 						while($row = mysqli_fetch_assoc($result))
    					{
					    	?>
					    	<option value=<?php echo $row['city_name']; ?> ><?php echo $row['city_name']; ?></option>';
					    <?php
					    }
					    ?>
                	?>
                </select>
            </div>
            </div>

		<div class="form-group row">
		  <label for="street"class="col-2 col-form-label">Street:</label>
		  <div class="col-10">
		  <input type="text" value="<?php echo $Street;	  ?>"class="form-control" id="street" name="street" required="">
		  </div>
		</div>

		<div class="form-group row">
		  <label for="zipcode"class="col-2 col-form-label">Zipcode:</label>
		  <div class="col-10">
		  <input type="text"value="<?php echo $zipcode;	  ?>"class="form-control" id="zipcode" name="zipcode"required="">
		  </div>
		</div>

	</div>
	<button class="btn btn-outline-primary" type="submit" name="add_user_update">Update</button>
	 
	  <a href='userList.php'>Go to list</a>
</form>
</div>
<?php
require './footer.php';
$conn->close();
?>