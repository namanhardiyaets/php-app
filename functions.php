<?php
//FUNCTION TO CREATE JSON FILE
	function createJSONfile($emparray)
	{
		$fileName = 'datafile-'.rand(10,100).'.json';	     
	     if(file_put_contents($fileName, json_encode($emparray)))
	     {
            header("Content-disposition: attachment; filename=\"".$fileName."\""); 
            readfile($fileName);
	     	echo $fileName." file created";
	     	echo "<a href='userList.php'>Go to list</a>";
            exit;
	     }
	}
//FUNCTION TO CREATE XML file
	function createXMLfile($emparray)
	{
       $filePath = 'datafile-'.rand(10,1000).'.xml';
       $dom = new DOMDocument('1.0', 'utf-8'); 
       $root = $dom->createElement('records'); 
       for($i=0; $i<count($emparray); $i++){
         
         $Id = $emparray[$i]['id'];  
         $compName = htmlspecialchars($emparray[$i]['company']);
         $firstName = $emparray[$i]['firstname']; 
         $lastName = $emparray[$i]['lastname']; 
         $email = $emparray[$i]['email']; 
         $city  = $emparray[$i]['city'];
         $street  = $emparray[$i]['street'];  
         $zipcode  = $emparray[$i]['zipcode'];  

         $record = $dom->createElement('record');
         $record->setAttribute('id', $Id);

         $CompName = $dom->createElement('company', $compName); 
         $record->appendChild($CompName);

         $FirstName = $dom->createElement('firstname', $firstName); 
         $record->appendChild($FirstName);

         $LastName = $dom->createElement('lastname', $lastName); 
         $record->appendChild($LastName);

         $Email = $dom->createElement('email', $email); 
         $record->appendChild($Email);

         $City = $dom->createElement('city', $city); 
         $record->appendChild($City);

         $Street = $dom->createElement('street', $street); 
         $record->appendChild($Street);

         $Zipcode = $dom->createElement('zipcode', $zipcode); 
         $record->appendChild($Zipcode);
         $root->appendChild($record);
       }
       $dom->appendChild($root);
       $dom->save($filePath);

        header("Content-disposition: attachment; filename=\"".$filePath."\""); 
        readfile($filePath);
        exit;
       echo $filePath." file created";
	   echo "<a href='userList.php'>Go to list</a>";
	}
?>