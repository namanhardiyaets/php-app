<?php
require './include/dbConnection.php';

function getCitys($conn) {
		$allCity = ("select * from city");
		$result = $conn->query($allCity);
    	$emparray = [];
		if ($result->num_rows==0) {
    	$emparray = [];
			
		}
		else{
		 	while($row = mysqli_fetch_assoc($result))
		    {
	        	$emparray[] = $row['city_name'];
		    }
		}
	return $emparray;
	}
// FOR ADD NEW USER
if (isset($_POST['add_user_submit'])) {
	$companyName = $_POST['name'];
	$firstName = $_POST['fname'];
	$lastName = $_POST['lname'];
	$email = $_POST['email'];
	$city = $_POST['city'];
	$streetName = $_POST['street'];
	$zipcodeName = $_POST['zipcode'];

//VALIDATIONS FOR EMAIL AND REQUIRED FIELDS TO ADD USER
	  if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
			$nameErr = "Only letters and white space allowed";
			echo '<div class=\'err\'><h3>'.$nameErr.'</h3></div>';
	  }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	  		$emailErr = "Invalid email format";
	  		echo '<div class=\'err\'><h3>'.$emailErr.'</h3></div>';
	  }elseif (empty($_POST["fname"])) {
	    	$firtNameErr = "Please enter first name";
	    	echo '<div class=\'err\'><h3>'.$firtNameErr.'</h3></div>';
	  }elseif (empty($_POST["lname"])) {
	    	$lastNameErr = "Please enter last name";
	    	echo '<div class=\'err\'><h3>'.$lastNameErr.'</h3></div>';
	  }elseif (empty($_POST["city"])) {
	    	$cityErr = "Please enter city name";
	    	echo '<div class=\'err\'><h3>'.$cityErr.'</h3></div>';
	  }elseif (empty($_POST["street"])) {
	    	$streetErr = "Please enter street no";
	    	echo '<div class=\'err\'><h3>'.$streetErr.'</h3></div>';
	  }elseif (empty($_POST["zipcode"])) {
	    	$zipErr = "Please zip-code";
	    	echo '<div class=\'err\'><h3>'.$zipErr.'</h3></div>';
	  }else{

		    $sql = "INSERT INTO records (company, firstname,lastname, email ,city ,street, zipcode)
				VALUES ('$companyName', '$firstName','$lastName','$email','$city','$streetName','$zipcodeName')";
			if ($conn->query($sql) === TRUE) {
			    echo "New record created successfully";
			} else {
			    echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
}

//TO UPDATE EXISTING USER

if (isset($_POST['add_user_update'])) {
	
	$Id = $_POST['id'];
	$companyName = $_POST['name'];
	$firstName = $_POST['fname'];
	$lastName = $_POST['lname'];
	$email = $_POST['email'];
	$city = $_POST['city'];
	$streetName = $_POST['street'];
	$zipcodeName = $_POST['zipcode'];

	//	VALIDATIONS FOR EMAIL AND REQUIRED FIELDS TO UPDATE USER
	if (!preg_match("/^[a-zA-Z ]*$/",$firstName)) {
			$nameErr = "Only letters and white space allowed";
		echo '<div class=\'err\'><h3>'.$nameErr.'</h3></div>';			
	  }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	  		$emailErr = "Invalid email format";
	  		echo '<div class=\'err\'><h3>'.$emailErr.'</h3></div>';
	  }elseif (empty($_POST["fname"])) {
    	$firtNameErr = "Please enter First name";
    	echo '<div class=\'err\'><h3>'.$firtNameErr.'</h3></div>';
      }elseif (empty($_POST["lname"])) {
	    	$lastNameErr = "Please enter last name";
	    	echo '<div class=\'err\'><h3>'.$lastNameErr.'</h3></div>';
	  }elseif (empty($_POST["city"])) {
	    	$cityErr = "Please enter city name";
	    	echo '<div class=\'err\'><h3>'.$cityErr.'</h3></div>';
	  }elseif (empty($_POST["street"])) {
	    	$streetErr = "Please enter street no";
	    	echo '<div class=\'err\'><h3>'.$streetErr.'</h3></div>';
	  }elseif (empty($_POST["zipcode"])) {
	    	$zipErr = "Please zip-code";
	    	echo '<div class=\'err\'><h3>'.$zipErr.'</h3></div>';
	  }else{

    $sql =  "UPDATE records set company = '$companyName', firstname = '$firstName', lastname = '$lastName', email = '$email', city = '$city', street = '$streetName', zipcode = '$zipcodeName' WHERE id = '$Id'";
	if ($conn->query($sql) === TRUE) {
	    echo "Record updated successfully";
	} else {
	    echo "Error: " . $sql . "<br>" . $conn->error;
	}
}
}

?>