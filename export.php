<?php
    require './include/dbConnection.php';
    require './functions.php';

    $sql = "select * from records";

    $result = mysqli_query($conn, $sql) or die("Error in Selecting " . mysqli_error($conn));
     //create an array
    $emparray = array();
    while($row =mysqli_fetch_assoc($result))
    {
        $emparray[] = $row;
    }
    //TO CREATE JSON FIME
	if($_GET['to']=='json')
	{
        createJSONfile($emparray);
		
	}
	//TO CREATE XML FILE    
	if($_GET['to']=='xml')
	{
	    createXMLfile($emparray);
	}

    $conn->close();
?>